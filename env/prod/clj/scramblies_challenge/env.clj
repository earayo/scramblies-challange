(ns scramblies-challenge.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[scramblies_challenge started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[scramblies_challenge has shut down successfully]=-"))
   :middleware identity})
