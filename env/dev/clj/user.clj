(ns user
  (:require [scramblies-challenge.config :refer [env]]
            [clojure.spec.alpha :as s]
            [expound.alpha :as expound]
            [mount.core :as mount]
            [scramblies-challenge.figwheel :refer [start-fw stop-fw cljs]]
            [scramblies-challenge.core :refer [start-app]]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(defn start []
  (mount/start-without #'scramblies-challenge.core/repl-server))

(defn stop []
  (mount/stop-except #'scramblies-challenge.core/repl-server))

(defn restart []
  (stop)
  (start))


