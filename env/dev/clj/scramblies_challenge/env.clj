(ns scramblies-challenge.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [scramblies-challenge.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[scramblies_challenge started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[scramblies_challenge has shut down successfully]=-"))
   :middleware wrap-dev})
