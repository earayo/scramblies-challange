(ns scramblies-challenge.core-test
  (:require [cljs.test :refer-macros [is are deftest testing use-fixtures run-tests]]
            [pjstadig.humane-test-output]
            [reagent.core :as r]
            [scramblies-challenge.core :as rc]))

(deftest scramble-page-test
  (r/render rc/scramble-page js/document.body)
  (is (nil? (.getElementById js/document "result"))))
