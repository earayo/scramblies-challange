(ns scramblies-challenge.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [scramblies-challenge.core-test]
            [scramblies-challenge.components.scramble-test]))

(enable-console-print!)

(doo-tests 'scramblies-challenge.core-test
           'scramblies-challenge.components.scramble-test)

