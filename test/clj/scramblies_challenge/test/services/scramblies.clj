(ns scramblies-challenge.test.services.scramblies
  (:require [clojure.test :refer :all]
            [scramblies-challenge.services.scramblies :as sc]))

(deftest scramble?-test
  (testing "Checking the success scramble"
    (is (true? (sc/scramble? "rekqodlw" "world")))
    (is (true? (sc/scramble?  "cedewaraaossoqqyt" "codewars"))))

  (testing "Checking the failed scramble"
    (testing "not contains the word"
      (is (false? (sc/scramble? "katas" "steak"))))))

