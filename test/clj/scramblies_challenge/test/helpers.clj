(ns scramblies-challenge.test.helpers
  (:require [cheshire.core :refer [parse-string]]))

(defn parse-response [body]
  (-> body slurp (parse-string true)))
