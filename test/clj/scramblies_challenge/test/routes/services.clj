(ns scramblies-challenge.test.routes.services
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [scramblies-challenge.handler :refer :all]
            [scramblies-challenge.test.helpers :as t-helpers]))

(deftest scramble-resource-test
  (testing "Testing #/api/scramble"
    (testing "Cheking with two valid words: It should response 200 and scramble response in true"
      (let [response (-> (request :get "/api/scramble")
                         (content-type "application/json")
                         (assoc :query-params {:str_1 "cedewaraaossoqqyt" :str_2 "codewars"})
                         (app))
            json-resp (t-helpers/parse-response (:body response))]
        (is (= 200 (:status response)))
        (is (true? (:scramble json-resp)))))

    (testing "Cheking with two invalid words: It should response 200 and scramble response in false"
      (let [response (-> (request :get "/api/scramble")
                         (content-type "application/json")
                         (assoc :query-params {:str_1 "cedewaraaossoqqyt" :str_2 "steak"})
                         (app))
            json-resp (t-helpers/parse-response (:body response))]
        (is (= 200 (:status response)))
        (is (false? (:scramble json-resp)))))

    (testing "not-found: Invalid path"
      (let [response (-> (request :get "/api/scamble")
                         (content-type "application/json")
                         (app))]
        (is (= 404 (:status response)))))

    (testing "It should failed because the two params are mandatories"
      (let [response (-> (request :get "/api/scramble")
                         (content-type "application/json")
                         (assoc :query-params {:str_1 "" :str_2 ""})
                         (app))
            json-resp (t-helpers/parse-response (:body response))]
        (is (= 400 (:status response)))))))
