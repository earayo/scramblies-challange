(ns scramblies-challenge.test.handler
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [scramblies-challenge.handler :refer :all]
            [scramblies-challenge.middleware.formats :as formats]
            [mount.core :as mount]))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'scramblies-challenge.config/env
                 #'scramblies-challenge.handler/app)
    (f)))

(deftest test-app
  (testing "main route"
    (let [response (app (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "not-found route"
    (let [response (app (request :get "/invalid"))]
      (is (= 404 (:status response))))))
