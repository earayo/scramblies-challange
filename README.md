# scramblies_challenge

The project's goal is build a full-stack app with clojure and clojure script to check
given two strings, one characters portion in the first string (`str_1`) can be rearranged to match in the second string (`str_2`).

It should return `true` if a portion of `str_1` characters can be rearranged to match `str_2`, otherwise returns `false`.

It was genated using Luminus version "3.10.19", the used templates to built was:

`swagger`: Used to write the resource's documentation

`reagent`: Used to build the UI.

## acknowledgment
Currently, I'm learning clojurescript. The following projects helped me much to build the UI using Reagent.

- The post located in https://dhruvp.github.io/2015/02/23/mailchimip-clojure/
- The examples in https://github.com/bensu/doo


## Where can I see it
The UI can be seen in https://scrambliechallange.herokuapp.com/

The API documentation can be seen in https://scrambliechallange.herokuapp.com/swagger-ui

## Prerequisites

You will need
- [Leiningen][1] 2.0 or above installed.
- [karma][2] 0.12.0 or above installed.
- [karma-clj-test][3] 0.1.0 installed.
- [slimerjs][4] 0.906.2 installed
- [karma-slimerjs-launcher][5] 1.1.0 installed

[1]: https://github.com/technomancy/leiningen
[2]: https://www.npmjs.com/package/karma
[3]: https://www.npmjs.com/package/karma-cljs-test
[4]: https://www.npmjs.com/package/slimerjs
[5]: https://www.npmjs.com/package/karma-slimerjs-launcher

## Running

First you need to build the `cljs` static files. Run:

    lein cljsbuild once

To start a web server for the application, run:

    lein run

## Testing

### Testing the API
To run all test cases in the API, you need to run:

    lein test

### Testing the UI
To run the test cases done in the UI, you need run:

    lein run ## Startup the server
    lein doo auto


## License

Copyright © 2018 Edward Alejandro Rayo Cotés
