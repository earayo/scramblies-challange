(ns scramblies-challenge.routes.services
  (:require [ring.util.http-response :refer :all]
            [compojure.api.sweet :refer :all]
            [schema.core :as s]
            [scramblies-challenge.services.scramblies :as scramble-service]))

(defapi service-routes
  {:swagger {:ui "/swagger-ui"
             :spec "/swagger.json"
             :data {:info {:version "1.0.0"
                           :title "Sample Scramble API"
                           :description "Scramble Services"}}}}
  
  (context "/api" []
           :tags ["scramble"]

           (GET "/scramble" []
                :summary      "returns true if a portion of str_1 characters can be rearranged to match str_2, otherwise returns false"
                :return       {:scramble s/Bool}
                :query-params [str_1 :- (s/constrained s/Str seq)
                               str_2 :- (s/constrained s/Str seq)]
                (ok {:scramble (scramble-service/scramble? str_1 str_2)}))))
