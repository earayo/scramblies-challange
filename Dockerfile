FROM java:8-alpine

COPY target/uberjar/scramblies_challenge.jar /scramblies_challenge/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/scramblies_challenge/app.jar"]
